echo *********************************
echo * RESTARTING all other services ...
echo *********************************
net start daa_core_connectivity
net start daa_core_connectivity_debit

echo *********************************
echo * RESTARTING all tomcat services ...
echo *********************************
net start ema_monitoring
net start ema_outbound
net start ema_cores
net start ema_files
net start ema_inbound_web
net start ema_inbound

echo *********************************
echo * TODO : stop / start queues
echo *********************************