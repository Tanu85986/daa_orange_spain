@echo off
setlocal enableextensions
set OUG_ENV=${INSTALL_PATH}\env_config\daa_connectivity
set IB_HOME=${INSTALL_PATH}\deliveries\connectivity_binaries-${com.ema.connecitivity.version}
set OUG_HOME=${INSTALL_PATH}\deliveries\connectivity_binaries-${com.ema.connecitivity.version}\wrapper
set SCRIPT_TOOLBOX_HOME=${ema_toolbox.path}
if "${install.connectivity.service}" == "Yes" (
if "${input.is.admin}" == "Yes" (
echo UG Install path is : %OUG_HOME%
echo UG Environment path is : %OUG_ENV%
cd %OUG_ENV% 

"%OUG_HOME%\wrapper.exe"  -r "%OUG_ENV%/wrapper.conf"
"%OUG_HOME%\wrapper.exe"  -it "%OUG_ENV%/wrapper.conf"
) else ( 
"%SCRIPT_TOOLBOX_HOME%/Install-MyDAAService.exe" "%OUG_ENV%/wrapper.conf" EMA_DaaSystem silent )
)
REM pause


