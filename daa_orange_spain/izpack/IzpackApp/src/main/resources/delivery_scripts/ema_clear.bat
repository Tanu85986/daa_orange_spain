@ECHO OFF

echo *********************************
echo * STOPPING ALL Tomcat SERVICES ...
echo *********************************
net stop ema_cores
net stop ema_files
net stop ema_inbound
net stop ema_inbound_web
net stop ema_monitoring
net stop ema_outbound
echo STOPPING ALL DAA SERVICES [Done]

echo *********************************
echo * REMOVING ALL DAA SERVICES ...
echo *********************************
net stop daa_core_connectivity
sc  delete  daa_core_connectivity

echo REMOVING ALL DAA SERVICES [Done]
echo REMOVING EMA folder ...
rmdir /S /Q D:\EMA
mkdir D:\EMA

echo *********************************
echo * Recreates tomcat symbolic links
echo *********************************
mklink /J "D:\EMA\tomcat_cores_catalina_localhost"        "C:\Program Files\Apache Software Foundation\tomcat_ema_cores\conf\Catalina\localhost"
mklink /J "D:\EMA\tomcat_files_catalina_localhost"        "C:\Program Files\Apache Software Foundation\tomcat_ema_files\conf\Catalina\localhost"
mklink /J "D:\EMA\tomcat_inbound_catalina_localhost"      "C:\Program Files\Apache Software Foundation\tomcat_ema_inbound\conf\Catalina\localhost"
mklink /J "D:\EMA\tomcat_inbound_web_catalina_localhost"  "C:\Program Files\Apache Software Foundation\tomcat_ema_inbound_web\conf\Catalina\localhost"
mklink /J "D:\EMA\tomcat_monitoring_catalina_localhost"   "C:\Program Files\Apache Software Foundation\tomcat_ema_monitoring\conf\Catalina\localhost"
mklink /J "D:\EMA\tomcat_outbound_catalina_localhost"     "C:\Program Files\Apache Software Foundation\tomcat_ema_outbound\conf\Catalina\localhost"

echo *********************************
echo * Removes all files deployment files
echo *********************************

DEL /F /Q "D:\EMA\tomcat_cores_catalina_localhost\*.xml
DEL /F /Q "D:\EMA\tomcat_files_catalina_localhost\*.xml
DEL /F /Q "D:\EMA\tomcat_inbound_catalina_localhost\*.xml
DEL /F /Q "D:\EMA\tomcat_inbound_web_catalina_localhost\*.xml
DEL /F /Q "D:\EMA\tomcat_monitoring_catalina_localhost\*.xml
DEL /F /Q "D:\EMA\tomcat_outbound_catalina_localhost\*.xml

echo *********************************
echo * RESTARTING all tomcat services ...
echo *********************************
net start ema_cores
net start ema_files
net start ema_inbound
net start ema_inbound_web
net start ema_monitoring
net start ema_outbound


echo RESTARTING all tomcat services [Done]



sleep 25
