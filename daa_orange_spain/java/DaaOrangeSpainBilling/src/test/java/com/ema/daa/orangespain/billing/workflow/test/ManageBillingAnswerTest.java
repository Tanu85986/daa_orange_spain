package com.ema.daa.orangespain.billing.workflow.test;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ema.daa.orangespain.billing.data.BillingData;
import com.ema.daa.util.billing.output.BillingOutput;
import com.ema.process.IProcessElement;
import com.ema.process.component.http.beans.HttpResponseDefinition;
import com.ema.process.component.xpath.BasicXpathComponent;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/test.spring.main.application.context.xml",
        "file:src/test/resources/test.spring.jdbc.context.xml",
        "file:src/main/resources/spring.process.flow.orangespain.billing.context.xml" })
public class ManageBillingAnswerTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManageBillingAnswerTest.class);

    @Resource(name = "billing.success.response")
    private String successResponse;

    @Resource(name = "billing.ko.response")
    private String koResponse;

    @Resource(name = "billing.unkn.response")
    private String unknResponse;

    @Resource(name = "billingXpathComponent")
    BasicXpathComponent<BillingData> billingXpathComponent;

    @Resource(name = "manageBillingAnswer")
    IProcessElement<BillingData> manageBillingAnswer;

    @Before
    public void initialize() {
        LOGGER.error(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }

    @Test
    public void testBillingResponseManager_404() throws Exception {
        BillingData testData = getBillingTestData(404, null, null);// NOT FOUND
        manageBillingAnswer.execute(testData);
        Assert.assertEquals("Return status should be -1 (UNKNOWN)", BillingOutput.STATUS_UKN,
                testData.getOutputData().getReturnStatus());
        Assert.assertEquals("ReturnCode should be 404", 404, testData.getOutputData().getReturnCode());
    }

    @Test
    public void testBillingResponseManager_OK() throws Exception {
        BillingData testData = getBillingTestData(successResponse);
        billingXpathComponent.execute(testData);
        testData.getServiceResponse().setHttpReturnCode(200);
        manageBillingAnswer.execute(testData);
        Assert.assertEquals("Return status should be 0 (OK)", BillingOutput.STATUS_OK,
                testData.getOutputData().getReturnStatus());
        Assert.assertEquals("ReturnCode should be 0", 0, testData.getOutputData().getReturnCode());
    }

    @Test
    public void testBillingResponseManager_knownErrorCode() throws Exception {
        BillingData testData = getBillingTestData(koResponse);
        billingXpathComponent.execute(testData);
        testData.getServiceResponse().setHttpReturnCode(200);
        manageBillingAnswer.execute(testData);
        Assert.assertEquals("Return status should be 1 (KO)", BillingOutput.STATUS_KO,
                testData.getOutputData().getReturnStatus());
        Assert.assertEquals("ReturnCode should be 99", 99, testData.getOutputData().getReturnCode());
    }

    @Test
    public void testBillingResponseManager_unknownErrorCode() throws Exception {
        BillingData testData = getBillingTestData(unknResponse);
        billingXpathComponent.execute(testData);
        testData.getServiceResponse().setHttpReturnCode(200);
        manageBillingAnswer.execute(testData);
        Assert.assertEquals("Return status should be -1 (UNKNOWN)", BillingOutput.STATUS_UKN,
                testData.getOutputData().getReturnStatus());
        Assert.assertEquals("ReturnCode should be 555", 555, testData.getOutputData().getReturnCode());
    }

    @Test
    public void testBillingResponseManager_invalidErrorCode() throws Exception {
        BillingData testData = getBillingTestData(200, "1aa0", null);
        manageBillingAnswer.execute(testData);
        Assert.assertEquals(BillingOutput.STATUS_UKN, testData.getOutputData().getReturnStatus());
    }

    private BillingData getBillingTestData(Integer httpReturnCode, String responseCode, String responseText) {
        BillingData testData = new BillingData();
        testData.setServiceResponse(new HttpResponseDefinition());
        testData.getServiceResponse().setHttpReturnCode(httpReturnCode);
        if (httpReturnCode != null) {
            testData.getBillingServiceResponse().setErrorCode(responseCode);
            testData.getBillingServiceResponse().setErrorDescription(responseText);
        }
        return testData;
    }

    private BillingData getBillingTestData(String responseBody) {
        BillingData testData = new BillingData();
        HttpResponseDefinition response = new HttpResponseDefinition();
        response.setBody(responseBody);
        testData.setServiceResponse(response);
        return testData;
    }

}
