package com.ema.daa.orangespain.billing.data;

public class BillingDataIn {

    private String rechargeDate;
    private Long daaBillingTransactionId;

    public final String getRechargeDate() {
        return rechargeDate;
    }

    public final void setRechargeDate(String rechargeDate) {
        this.rechargeDate = rechargeDate;
    }

    public final Long getDaaBillingTransactionId() {
        return daaBillingTransactionId;
    }

    public final void setDaaBillingTransactionId(Long daaBillingTransactionId) {
        this.daaBillingTransactionId = daaBillingTransactionId;
    }

    @Override
    public final String toString() {
        return "BillingDataIn [rechargeDate=" + rechargeDate + ", daaBillingTransactionId=" + daaBillingTransactionId
                + "]";
    }

}
