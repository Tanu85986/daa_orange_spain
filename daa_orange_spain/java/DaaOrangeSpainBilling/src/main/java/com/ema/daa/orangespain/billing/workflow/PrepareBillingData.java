package com.ema.daa.orangespain.billing.workflow;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ema.daa.orangespain.billing.data.BillingData;
import com.ema.process.IProcessElement;

public class PrepareBillingData implements IProcessElement<BillingData> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PrepareBillingData.class);
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    @Override
    public final void execute(BillingData data) throws Exception {
        prepareData(data);
    }

    private void prepareData(BillingData data) {
        LOGGER.debug("Preparing billing data for transaction id <{}>",
                data.getInputData().getDaaBillingTransactionId());
        data.getInputData().setRechargeDate(dateTimeFormatter.format(ZonedDateTime.now()));
    }
}
