package com.ema.daa.orange.spain.sendsms.tasks.test;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ema.daa.orange.spain.sendsms.data.SendSmsData;
import com.ema.process.IProcessElement;
import com.ema.process.component.http.beans.HttpResponseDefinition;
import com.ema.process.component.xpath.BasicXpathComponent;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/test.spring.main.application.context.xml",
        "file:src/test/resources/test.spring.jdbc.context.xml",
        "file:src/main/resources/spring.process.flow.orange.spain.sendsms.context.xml" })
public class ManageSendSmsAnswerTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManageSendSmsAnswerTest.class);

    @Resource(name = "sendSms.success.response")
    private String successResponse;

    @Resource(name = "sendSms.failure.response")
    private String failureResponse;

    @Resource(name = "sendSmsXPathComponent")
    BasicXpathComponent<SendSmsData> sendSmsXPathComponent;

    @Resource(name = "manageSendSmsAnswer")
    IProcessElement<SendSmsData> manageSendSmsAnswer;

    @Before
    public void initialize() {
        LOGGER.error(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }

    @Test
    public void testSendSmsResponseManager_OK() throws Exception {
        SendSmsData testData = getSendSmsTestData(successResponse);
        sendSmsXPathComponent.execute(testData);
        testData.getServiceResponse().setHttpReturnCode(200);
        manageSendSmsAnswer.execute(testData);
        Assert.assertEquals("ReturnCode should be 0000", "0000", testData.getServiceOutputData().getErrorCode());
        Assert.assertEquals("ErrorDescription should be Success", "Success",
                testData.getServiceOutputData().getErrorDescription());
    }

    @Test
    public void testSendSmsResponseManager_Fail() throws Exception {
        SendSmsData testData = getSendSmsTestData(failureResponse);
        sendSmsXPathComponent.execute(testData);
        testData.getServiceResponse().setHttpReturnCode(200);
        manageSendSmsAnswer.execute(testData);
        Assert.assertNotEquals("ReturnCode should not be 0000", "0000", testData.getServiceOutputData().getErrorCode());
        Assert.assertEquals("ErrorDescription should be Error", "Error",
                testData.getServiceOutputData().getErrorDescription());
    }

    private SendSmsData getSendSmsTestData(String responseBody) {
        SendSmsData testData = new SendSmsData();
        HttpResponseDefinition response = new HttpResponseDefinition();
        response.setBody(responseBody);
        testData.setServiceResponse(response);
        return testData;
    }
}
