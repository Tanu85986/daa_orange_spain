package com.ema.daa.orange.spain.sendsms.data;

import java.util.HashMap;
import java.util.Map;

import com.ema.daa.orange.spain.sendsms.jms.DaaNotification;
import com.ema.daa.util.billing.output.BillingOutput;
import com.ema.process.component.http.beans.HttpResponseDefinition;
import com.ema.process.service.IServiceData;

public class SendSmsData implements IServiceData<SendSmsDataIn, BillingOutput> {
    private DaaNotification daaNotification;
    private SendSmsDataIn inputData;
    private BillingOutput outputData = new BillingOutput();
    private String serviceRequest;
    private HttpResponseDefinition serviceResponse;
    private OrangeSpainResponse serviceOutputData = new OrangeSpainResponse();

    private Map<String, Object> otherData = new HashMap<>();

    public final Map<String, Object> getOtherData() {
        return otherData;
    }

    public final void setOtherData(Map<String, Object> otherData) {
        this.otherData = otherData;
    }

    public final OrangeSpainResponse getServiceOutputData() {
        return serviceOutputData;
    }

    public final void setServiceOutputData(OrangeSpainResponse serviceOutputData) {
        this.serviceOutputData = serviceOutputData;
    }

    public final DaaNotification getDaaNotification() {
        return daaNotification;
    }

    public final void setDaaNotification(DaaNotification daaNotification) {
        this.daaNotification = daaNotification;
    }

    public final SendSmsDataIn getInputData() {
        return inputData;
    }

    @Override
    public final void setInputData(SendSmsDataIn inputData) {
        this.inputData = inputData;
    }

    @Override
    public final BillingOutput getOutputData() {
        return outputData;
    }

    public final void setOutputData(BillingOutput outputData) {
        this.outputData = outputData;
    }

    public final String getServiceRequest() {
        return serviceRequest;
    }

    public final void setServiceRequest(String serviceRequest) {
        this.serviceRequest = serviceRequest;
    }

    public final HttpResponseDefinition getServiceResponse() {
        return serviceResponse;
    }

    public final void setServiceResponse(HttpResponseDefinition serviceResponse) {
        this.serviceResponse = serviceResponse;
    }

}
